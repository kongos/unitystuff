# Stuff for unity
 * `Testing Vibration.unity` shows how to issue vibrations on a `rooted` android phone
 * `HeartBeat.unity` shows how to change the pitch programatically to simulate a speedier heart rate. This is achived since pitch in Unity changes both pitch and tempo.

##Usage
### `Testing Vibration.unity`
#### Running
Make sure your Android phone is rooted, connect to the same wifi and open up port `5555` on your phone.

To do this easily, download and run [ADB Konnect](https://play.google.com/store/apps/details?id=com.rockolabs.adbkonnect "ADB Konnect") or similar.

When all is good, run `Testing Vibration.unity` and see what can be done.

#### Troubleshooting / Things to think about
There is a delay of 1.5 seconds before any vibrations can be issues so be a good developer and wait.

Sometimes there are issues with connecting to the phone, if so one can manually connect to a phone by issuing `adb connect PHONES_IP`

####Closing
Simply press play again

### `HeartBeat.unity`
#### Running

Run `HeartBeat.unity` and see what can be done.

#### Troubleshooting / Things to thnk about
True tempo shifting can be achieved with third party `.dll` such as [Dirac3](http://dirac.dspdimension.com/Dirac3_Technology_Home_Page/Dirac3_Technology.html "Dirac3").

####Closing
Simply press play again
