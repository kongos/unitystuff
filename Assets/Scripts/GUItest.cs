﻿using UnityEngine;

public class GUItest : MonoBehaviour
{

		void OnGUI ()
		{
		if (GUI.Button (new Rect (20, 10, 800, 20), "FeedbackVibrations.VIBRATION_LENGTH.SHORT")) {
						FeedbackManager.TriggerVibrate (FeedbackVibrations.VIBRATION_LENGTH.SHORT);
				}
		
		if (GUI.Button (new Rect (20, 40, 800, 20), "FeedbackVibrations.VIBRATION_LENGTH.MEDIUM")) {
						FeedbackManager.TriggerVibrate (FeedbackVibrations.VIBRATION_LENGTH.MEDIUM);
				}

		if (GUI.Button (new Rect (20, 70, 800, 20), "FeedbackVibrations.VIBRATION_LENGTH.LONG")) {
						FeedbackManager.TriggerVibrate (FeedbackVibrations.VIBRATION_LENGTH.LONG);
				}
		}
}
