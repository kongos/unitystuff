using UnityEngine;
using System;
using System.IO;
using System.Diagnostics;

public class FeedbackVibrations : MonoBehaviour
{
/*
 * This class creates a configuration file for what ip the current 
 * android phone has, later it connects to it and since it is
 * registered with FeedbackManager.Vibrate one can call vibrate with it.
 */
		private string su = "su -";
		private Process adb;
		private StreamWriter adbWriter;
		private string arguments = "-s 192.168.1.12:5555 shell";
		private string adbPreferences = "adbPreferences";

		/* 
		 * Enum to hold different vibrations
		 */
		public enum VIBRATION_LENGTH
		{
				SHORT,
				MEDIUM,
				LONG
		}
		
		/*
		 * Since we hold the configuration in a configuration file called adbPreferences 
		 * the ip can be changed after source is built
		 */
		void Start ()
		{
				FeedbackManager.Vibrate += Vibrate;
				if (File.Exists (adbPreferences)) {
						StreamReader sr = File.OpenText (adbPreferences);
						arguments = sr.ReadLine ();
						sr.Close ();
						UnityEngine.Debug.Log ("arguments " + arguments + "loaded");
				} else {
						StreamWriter sw = File.CreateText (adbPreferences);
						sw.WriteLine (arguments);
						sw.Flush ();
						sw.Close ();
						UnityEngine.Debug.Log (arguments);
				}
				
				//Delay invokation since it needs to connect first 	
				Invoke ("AdbShell", 1.5F);
				Process adbConnect = new Process ();
				adbConnect.StartInfo.FileName = "adb.exe";
				string adbConnInfo = "connect " + arguments.Substring (arguments.IndexOf (' '), arguments.IndexOf (':') - 2);
				UnityEngine.Debug.Log (adbConnInfo);
				adbConnect.StartInfo.Arguments = adbConnInfo;
				adbConnect.StartInfo.UseShellExecute = false;
				adbConnect.StartInfo.CreateNoWindow = true;
				adbConnect.Start ();
		}
		/*
		 * Creates a connection to a Android phone with adb and gains root
		 */
		void AdbShell ()
		{
				adb = new Process ();
				try {
						adb.StartInfo.FileName = "adb.exe";
						adb.StartInfo.Arguments = arguments;
						adb.StartInfo.UseShellExecute = false;
						adb.StartInfo.CreateNoWindow = true;
						adb.StartInfo.RedirectStandardInput = true;
						adb.StartInfo.RedirectStandardOutput = false;
						if (adb.Start ()) {
								adbWriter = adb.StandardInput;
								adbWriter.WriteLine (su);	
						}
						
				} catch (Exception e) {
						UnityEngine.Debug.Log (e);
			
				}
		}

		/*
		 * Recieves the vibration call and writes to Android hardware
		 */
		void Vibrate (FeedbackVibrations.VIBRATION_LENGTH length)
		{
				try {
						switch (length) {
						case VIBRATION_LENGTH.LONG:
								adbWriter.WriteLine ("echo 500 > /sys/devices/virtual/timed_output/vibrator/enable");
								break;

						case VIBRATION_LENGTH.MEDIUM:
								adbWriter.WriteLine ("echo 300 > /sys/devices/virtual/timed_output/vibrator/enable");
								break;

						case VIBRATION_LENGTH.SHORT:
								adbWriter.WriteLine ("echo 100 > /sys/devices/virtual/timed_output/vibrator/enable");
								break;
						default:
								UnityEngine.Debug.Log ("VIBRATION LENGTH NOT SUPPORTED");
								break;

						}

				} catch (Exception e) {
						UnityEngine.Debug.Log (e);
				}
		}
}
