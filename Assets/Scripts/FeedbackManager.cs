﻿public static class FeedbackManager
{
	
		/**
	 * FeedbackManager; This is for delegating Feedback to their corresponding classes
	 * To register a Feedback simply add to Start() in corresponding class;
	 * Feedback.EVENT +=EVENT;
	 * Now corresponding function will be delegated to this class so corresponding function needs
	 * to be added. 
	 * Call the function from anywhere with Feedback.TriggerEVENT(...) it will now be ran in the classes 
	 * that have been said to use this
	 */
	
		public delegate void Vibrator (FeedbackVibrations.VIBRATION_LENGTH length);

		public static event Vibrator Vibrate;

		public static void TriggerVibrate (FeedbackVibrations.VIBRATION_LENGTH length)
		{
				if (Vibrate != null) {
						Vibrate (length);
				}
		}

}
