﻿using UnityEngine;
using System.Collections;

public class ChangePitch : MonoBehaviour
{
		/*
		 *Changes pitch on the gameObjects audiosource
	 	 */
		public float minPitch, maxPitch;
		private AudioSource myAudioSource;
		
		// Use this for initialization
		void Start ()
		{
				myAudioSource = gameObject.GetComponent<AudioSource> ();
				if (minPitch < -3.0F) {
						Debug.Log ("Minpitch was too low");
						Application.Quit ();
				} else if (maxPitch > 3.0F) {
						Debug.Log ("Maxpitch was too high");
						Application.Quit ();
				}
		}
	
		void OnGUI ()
		{
				if (GUI.Button (new Rect (20, 10, 800, 20), "Lower")) {
						if (myAudioSource.pitch > minPitch) {
								myAudioSource.pitch -= 0.1F;
						}
						Debug.Log ("Pitch = " + myAudioSource.pitch);
				}
		
				if (GUI.Button (new Rect (20, 40, 800, 20), "ZERO OUT")) {
						myAudioSource.pitch = 1.0F;
						Debug.Log ("Pitch = " + myAudioSource.pitch);

				}
		
				if (GUI.Button (new Rect (20, 70, 800, 20), "Raise")) {
						if (myAudioSource.pitch < maxPitch) {
								myAudioSource.pitch += 0.1F;
						}
						Debug.Log ("Pitch = " + myAudioSource.pitch);

				}
		}
}
